/* 
 * unshare_chroot 
 *
 * Copyright (C) 2020 Iris <iris@fblog.ch>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *
 *
 *
 * Copyright (C) 2009 Mikhail Gusarov <dottedmag@dottedmag.net>
 *
 * https://github.com/karelzak/util-linux
 * https://github.com/karelzak/util-linux/blob/master/sys-utils/unshare.c
 *
 */ 

#define _GNU_SOURCE
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sched.h>
#include <stdarg.h>
#include <stdbool.h> 
#include <string.h>
#include <errno.h>
#include <getopt.h>
#include <limits.h>
#include <stdint.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <stddef.h>
#include <unistd.h>
#include <sys/mount.h>
#include <assert.h>
#include <mntent.h>

#define VERSION "0.0.3"

/* from util-linux/include/pathnames.h */
#define _PATH_PROC_UIDMAP		 "/proc/self/uid_map"
#define _PATH_PROC_GIDMAP		 "/proc/self/gid_map"
#define _PATH_PROC_SETGROUPS	 "/proc/self/setgroups"

#define err(E, FMT...) errmsg(1, E, 1, FMT)

/* bindmnt element - for list */
struct bindmnt {
	bool ro;
	char *src;
	char *dst;
	struct bindmnt *next;
};

/* force */
static bool force = false;

/* mk_parent - make parent directories */
static bool mk_parent = false;


static void
usage(char *pname)
{
	fprintf(stderr, "Usage: %s [options] NEWROOT [COMMAND [ARG]...]\n", pname);
	fprintf(stderr, "Options can be:\n");
	fprintf(stderr, "  -h, --help                 display this help\n");
	fprintf(stderr, "  -f, --force                force, not exit on error\n");
	fprintf(stderr, "  -p, --parents              make parent directories as needed\n");
	fprintf(stderr, "  -M, --mount src[:dest]     bind mounts <src> into <NEWROOT>/<dest>\n");
	fprintf(stderr, "                             if <dest> is not specified, <src> is used as <dest>\n"); 
	fprintf(stderr, "  -m, --mount-ro src[:dest]  same as --mount|-M, but read-only\n");
	fprintf(stderr, "  -u, --uid UID              map user id to UID\n");
	fprintf(stderr, "  -g, --gid GID              map group id to GID\n");
	fprintf(stderr, "  -V, --version              display version and exit\n");
}


static void
errmsg(char doexit, int excode, char adderr, const char *fmt, ...)
{
	fprintf(stderr, "%s: ", program_invocation_short_name);
	if (fmt != NULL) {
		va_list argp;
		va_start(argp, fmt);
		vfprintf(stderr, fmt, argp);
		va_end(argp);
		if (adderr)
			fprintf(stderr, ": ");
	}
	if (adderr)
		fprintf(stderr, "%m");
	fprintf(stderr, "\n");
	if (doexit)
		exit(excode);
}

inline static int
rmkdir(const char *path, mode_t mode)
{
	const char *p;
	char       *temp;

	temp = calloc(1, strlen(path)+1);
	p = path;

	while ((p = strchr(p, '/')) != NULL)
	{
		if ( p == path && p[0] == '/' )
		{
			p++;
			continue;
		}
		memcpy(temp, path, p-path);
		temp[p-path] = '\0';
		if (mkdir(temp, mode) != 0)
		{
			if (errno != EEXIST)
			{
				free(temp);
				return errno;
			}
		}
		p++;
	}
	free(temp);

	if (mkdir(path, mode) != 0)
	{
		if (errno != EEXIST)
		  return errno;
	}

	return 0;
}

inline static int
mount_options(const char *mp)
{
	FILE *f = NULL;
	struct mntent *mntent;
	unsigned long flags = 0;

	f = setmntent("/proc/self/mounts" , "r");
	while ( (mntent = getmntent(f)) )
	{
		if ( strcmp(mp, mntent->mnt_dir) >= 0 )
		{
			if (hasmntopt(mntent, "nosuid"))				flags = flags | MS_NOSUID;
			if (hasmntopt(mntent, "nodev"))				flags = flags | MS_NODEV;
			if (hasmntopt(mntent, "noexec"))				flags = flags | MS_NOEXEC;
			if (hasmntopt(mntent, "sync"))				flags = flags | MS_SYNCHRONOUS;
			if (hasmntopt(mntent, "dirsync"))			flags = flags | MS_DIRSYNC;
			if (hasmntopt(mntent, "noatime"))			flags = flags | MS_NOATIME;
			if (hasmntopt(mntent, "nodiratime"))		flags = flags | MS_NODIRATIME;
			if (hasmntopt(mntent, "mand"))				flags = flags | MS_MANDLOCK;
			if (hasmntopt(mntent, "relatime"))			flags = flags | MS_RELATIME;
			if (hasmntopt(mntent, "strictatime"))		flags = flags | MS_STRICTATIME;
			//if (hasmntopt(mntent, "rbind"))			flags = flags | MS_BIND | MS_RECURSIVE;
			//if (hasmntopt(mntent, "union"))			flags = flags | MS_UNION;
			if (hasmntopt(mntent, "bind"))				flags = flags | MS_BIND;
			if (hasmntopt(mntent, "move"))				flags = flags | MS_MOVE;
			if (hasmntopt(mntent, "shared"))				flags = flags | MS_SHARED;
			if (hasmntopt(mntent, "slave"))				flags = flags | MS_SLAVE;
			if (hasmntopt(mntent, "private"))			flags = flags | MS_PRIVATE;
			if (hasmntopt(mntent, "unbindable"))		flags = flags | MS_UNBINDABLE;
			//if (hasmntopt(mntent, "rshared"))			flags = flags | MS_SHARED | MS_RECURSIVE;
			//if (hasmntopt(mntent, "rslave"))			flags = flags | MS_SLAVE | MS_RECURSIVE;
			//if (hasmntopt(mntent, "rprivate"))		flags = flags | MS_PRIVATE | MS_RECURSIVE;
			//if (hasmntopt(mntent, "runbindable"))	flags = flags | MS_UNBINDABLE | MS_RECURSIVE;
			if (hasmntopt(mntent, "ro"))					flags = flags | MS_RDONLY;
		}
		endmntent(f);
	 	return flags;
	}
	endmntent(f);
	return 0;
}

inline static int
bind_mount(const char *src, const char * dest)
{
	int         ret;
	struct stat info;

	if ( mk_parent )
	{
		if( stat( dest, &info ) != 0 )
		{
			ret = rmkdir(dest, 0770);
			if ( ret != 0 )
			{
				fprintf(stderr, "Error: cannot create directory %s, %s\n", dest, strerror(ret));
				return ret;
			}
		}
		else if ((info.st_mode & S_IFMT) != S_IFDIR) /* S_ISDIR(t->st_mode)==0) */
		{
			fprintf(stderr, "%s is no directory\n", dest );
			return -1;
		}
	}

	ret = mount(src, dest, NULL, MS_BIND|MS_REC, NULL);
	if ( ret == -1 )
		fprintf(stderr, "mount %s %s failed: %s\n", src, dest, strerror(errno));
	return ret;
}

inline static int
remount_ro(const char * mp)
{
	int           ret;
	unsigned long flags;

	flags = mount_options(mp);

	ret = mount(NULL, mp, NULL, MS_REMOUNT|MS_RDONLY|MS_BIND|flags, NULL);
	if ( ret == -1 )
		fprintf(stderr, "remount ro %s failed: %s\n", mp, strerror(errno));
	return ret;
}

static void
add_bindmnt(struct bindmnt **lst, bool ro, char *bm)
{
	char           *src, *dst;
	int 	         pos;
	struct bindmnt *pnew;

	src = strdup(bm);
	dst = strchr(bm, ':');
	
	if (dst != NULL && *dst == '\0') 
		err(EINVAL, "invalid argument for bind-mount: %s", bm);
	else if (dst == NULL)
	  dst = strdup(bm);
	else
	{
		dst++;
		pos = dst - bm;
		src[pos-1] = '\0';
	}

	while( *lst != NULL ) 
		lst = &(*lst)->next;

	pnew = malloc(sizeof(*pnew));
	pnew->next = NULL;
	pnew->ro = ro;
	pnew->src = src;
	pnew->dst = dst;

	*lst = pnew;
}	 

static void fix_path(char *path)
{
	char *p = path;
	while ((p = strstr(p, "//")) != NULL)
	  memmove(p, p+1, strlen(p));
}

static void 
mount_bindmnt(const struct bindmnt *p, const char *target)
{
	char mp[PATH_MAX-1];
	
	for( ; p != NULL ; p = p->next )
	{
		sprintf(mp, "%s/%s", target, p->dst);
		fix_path(mp);
		if ( bind_mount(p->src, mp) == -1 && !force)
			exit(errno);
	}
}	

static void
mount_bindmnt_ro(const struct bindmnt *p, const char *target)
{
	for( ; p != NULL ; p = p->next )
	{
		if ( p->ro )
		{
			if ( remount_ro(p->dst) == -1 && !force) 
			  exit(errno);
	  	}
  	}
}


/* xusleep() - from util-linux/include/c.h */
static inline int 
xusleep(useconds_t usec)
{
	struct timespec waittime = {
		.tv_sec	=  usec / 1000000L,
		.tv_nsec  = (usec % 1000000L) * 1000
	};
	return nanosleep(&waittime, NULL);
}

/* write_all() - from util-linux/include/all-io.h */
static inline int 
write_all(int fd, const void *buf, size_t count)
{
	while (count) {
		ssize_t tmp;

		errno = 0;
		tmp = write(fd, buf, count);
		if (tmp > 0) {
			count -= tmp;
			if (count)
				buf = (const void *) ((const char *) buf + tmp);
		} else if (errno != EINTR && errno != EAGAIN)
			return -1;
		if (errno == EAGAIN)  /* Try later, *sigh* */
			xusleep(250000);
	}
	return 0;
}

static void 
setgroups_deny()
{
	const char *file = _PATH_PROC_SETGROUPS;
	const char *cmd  = "deny";
	int fd;

	fd = open(file, O_WRONLY);
	if (fd < 0) {
		if (errno == ENOENT) 
			return;
		err(EXIT_FAILURE, "cannot open %s", file);
	}

	if (write_all(fd, cmd, strlen(cmd)))
		err(EXIT_FAILURE, "write failed %s", file);
	close(fd);
}

/* xasprintf() - from util-linux/include/xalloc.h */
static inline
__attribute__((__format__(printf, 2, 3)))
int xasprintf(char **strp, const char *fmt, ...)
{
	int ret;
	va_list args;

	va_start(args, fmt);
	ret = vasprintf(&(*strp), fmt, args);
	va_end(args);
	if (ret < 0)
		err(EXIT_FAILURE, "cannot allocate string");
	return ret;
}

/* map_id() - from util-linux/sys-utils/unshare.c */
static void 
map_id(const char *file, uint32_t from, uint32_t to)
{
	char *buf;
	int  fd;

	fd = open(file, O_WRONLY);
	if (fd < 0)
		 err(EXIT_FAILURE, "cannot open %s", file);

	xasprintf(&buf, "%u %u 1", from, to);
	if (write_all(fd, buf, strlen(buf)))
		err(EXIT_FAILURE, "write failed %s", file);
	free(buf);
	close(fd);
}











int
main(int argc, char *argv[])
{
	struct bindmnt *bindmnt = NULL;
	char *prog_name;
	int c;
	char *rootfs, *cmd;
	char **cmdargv;

	uid_t mapuser = -1, real_euid = geteuid();
   gid_t mapgroup = -1, real_egid = getegid();


	prog_name = argv[0];

	const struct option longopts[] = {
		{ "force",		no_argument,		 NULL, 'f' },
		{ "parent",		no_argument,		 NULL, 'p' },
  		{ "mount",		required_argument, NULL, 'M' },
		{ "mount-ro",	required_argument, NULL, 'm' },
		{ "uid",       required_argument, NULL, 'u' },
		{ "gid",       required_argument, NULL, 'g' },
		{ "help",		no_argument,		 NULL, 'h' },
		{ "version",	no_argument,		 NULL, 'V' },
		{ NULL, 0, NULL, 0 }
	};

	prog_name = argv[0];

	/* parse options */
	while ((c = getopt_long(argc, argv, "fpM:m:u:g:hV", longopts, NULL)) != -1) {
		switch (c) {
			case 'f':
				force = true;
				break;
			case 'p':
				mk_parent = true;
				break;
			case 'M':
				add_bindmnt(&bindmnt, false, optarg);
				break;
			case 'm':
		  		add_bindmnt(&bindmnt, true, optarg);
		  		break;
			case 'u':
				mapuser = atoi(optarg);
				break;
			case 'g':
				mapgroup = atoi(optarg);
				break;
			case 'h':
				usage(prog_name);
				exit(EXIT_SUCCESS);	
			case 'V':
				printf("Version: %s\n", VERSION);
				exit(EXIT_SUCCESS);
			default:
				usage(argv[0]);
				exit(EXIT_FAILURE);
		}
	}
	argc -= optind;
	argv += optind;

	if (argc < 2) {
		usage(prog_name);
		exit(EXIT_FAILURE);
	}

	rootfs = argv[0]; 
	cmd = argv[1]; 
	cmdargv = argv + 1; 


	/* rootfs == / not allowed */
	if (strcmp(rootfs, "/") == 0) {
		err(EINVAL, "/ not allowd for NEWROOT");
	}

	/* absolute pathname ... */
	if (rootfs[0] != '/') {
		char cwd[PATH_MAX-1];
		if ( getcwd(cwd, sizeof(cwd)) == NULL ) {
			err(EXIT_FAILURE, "cwd failed" );
		}
		asprintf(&rootfs, "%s/%s", cwd, rootfs);
	} 

	//
	//  unshare()
	//
	int unshare_flags = 0;
	//uid_t mapuser = -1, real_euid = geteuid();
	//gid_t mapgroup = -1, real_egid = getegid();
	
	unshare_flags = CLONE_NEWUSER | CLONE_NEWNS;
	//unshare_flags = CLONE_NEWUSER|CLONE_NEWNS|CLONE_NEWIPC|CLONE_NEWUTS;

	if (unshare(unshare_flags) == -1)
		err(EXIT_FAILURE, "unshare failed");

	/* propagation = rslave */
	if (mount("none", "/", NULL, MS_REC | MS_SLAVE, NULL) != 0)
		err(EXIT_FAILURE, "cannot change root filesystem propagation");

	if ( mapuser != -1 )
		map_id(_PATH_PROC_UIDMAP, mapuser, real_euid);
	if ( mapgroup != -1 )
	{
		setgroups_deny();
		map_id(_PATH_PROC_GIDMAP, mapgroup, real_egid);
	}

	//
	// mount & chroot
	//
	add_bindmnt(&bindmnt, false, "/proc:/proc");
	add_bindmnt(&bindmnt, false, "/dev:/dev");
	add_bindmnt(&bindmnt, false, "/sys:/sys");
	mount_bindmnt(bindmnt, rootfs);

	/* chroot */
	if (chdir(rootfs) == -1)
		err(EXIT_FAILURE, "chdir to %s failed", rootfs);
	
	if (mount(".", "/", NULL, MS_BIND, NULL) == -1)
		err(EXIT_FAILURE, "Failed to move %s as rootfs", rootfs);

	if (chroot(".") == -1)
		err(EXIT_FAILURE, "chroot to %s failed", rootfs);

	/* remount ro */
	mount_bindmnt_ro(bindmnt, rootfs);

	/* execvp */
	if (execvp(cmd, cmdargv) == -1)
		err(EXIT_FAILURE, "Failed to execute command %s", cmd);

	exit(EXIT_FAILURE);
}


// vim: ts=3:sw=3:
